# Tugas 2 PPW Paket A
>Project by Kelompok 7 PPW-C

[![pipeline status](https://gitlab.com/naufal.ihsan/Tugas-2/badges/master/pipeline.svg)](https://gitlab.com/naufal.ihsan/Tugas-2/commits/master)  [![coverage report](https://gitlab.com/naufal.ihsan/Tugas-2/badges/master/coverage.svg)]
(https://gitlab.com/naufal.ihsan/Tugas-2/commits/master)

  Pada project kali ini membuat fitur terkait dengan role mahasiswa.
  1. Halaman Login dan Status
  2. Halaman Profile
  3. Halaman Riwayat
  4. Halaman Cari Mahasiswa


##  Team

1. Naufal Ihsan Pratama
2. Fata Nugraha
3. Deana Almira Putri
4. Muhammad Davin Ramayuda


## Gitlab and HerokuApp

  Gitlab repository : [https://gitlab.com/naufal.ihsan/Tugas-2](https://gitlab.com/naufal.ihsan/Tugas-2) <br/>
  Heroku app        : [https://collegestudent.herokuapp.com/](https://collegestudent.herokuapp.com/)


## Developing

  ```bash
    $ git clone https://gitlab.com/naufal.ihsan/Tugas-2.git
    $ cd Tugas-2/
  ```

  [![forthebadge](http://forthebadge.com/badges/built-by-developers.svg)](http://forthebadge.com)
