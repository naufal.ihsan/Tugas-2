def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']
    return data

def serialize_mahasiswa_obj(obj):
    print(obj.kode_identitas)
    res = {
        'npm': obj.kode_identitas,
        'name': (obj.first_name or '') + ' ' + (obj.last_name or ''),
        'image_url': obj.image_url,
        'email': obj.email,
        'profile_url': obj.profile_url,
        'expertises': [],
        'batch': obj.batch,
    }

    for item in obj.expertises.all():
        res['expertises'].append({
            'name': item.expert,
        })

    return res
