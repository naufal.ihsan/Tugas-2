from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .api_csui import get_access_token, verify_user
from django.contrib.auth.models import User
from .models import Profile

class SSOAuthBackend:
    """
        auth an user using UI SSO
    """
    def authenticate(self, request, username=None, password=None):
        token = get_access_token(username, password)
        if token is None:
            return None

        user = verify_user(token)
        npm = user['identity_number']

        user = User.objects.filter(username=npm).first()
        if user is not None:
            return user

        user = User.objects.create_user(npm, npm+'@localhost', '__random123__')
        profile = Profile.objects.create(kode_identitas=npm, user=user)
        return user

    def get_user(self, id):
        return User.objects.get(pk=id)
