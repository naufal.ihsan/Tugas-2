from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    kode_identitas = models.CharField(max_length=20, primary_key=True, )

    first_name = models.CharField('Nama Depan', max_length=255, null=True, blank=True)
    last_name = models.CharField('Nama Belakang', max_length=255, null=True, blank=True)
    image_url = models.URLField('Foto', null=True, blank=True)
    email = models.EmailField('Email', null=True, blank=True)
    profile_url = models.URLField('Profile LinkedIn', null=True, blank=True)
    batch = models.IntegerField('Angkatan', null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Expertise(models.Model):
    user = models.ForeignKey(Profile, related_name="expertises")
    expert = models.CharField('Bahasa Pemrograman',max_length = 27)
    level  = models.CharField('Tingkatan', max_length = 27)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
    user = models.ForeignKey(User)
    status = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Statuses"
        ordering = ['-created_at']

class Comment(models.Model):

    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    status = models.ForeignKey(
        Status,
        on_delete=models.CASCADE,
        related_name="comments")

    class Meta:
        ordering = ['created_at']
