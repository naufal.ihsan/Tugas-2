from random import randint
import json

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.conf import settings

from mahasiswa.models import Profile, Expertise


class Command(BaseCommand):
    help = 'Load fixture into database'

    def handle(self, *args, **options):
        FIXTURE_DIR = settings.BASE_DIR + '/mahasiswa/fixtures/dummy_mahasiswa.json'
        items = json.loads(open(FIXTURE_DIR).read())

        done = []

        for item in items:
            if item['kode_identitas'] in done:
                continue
            done.append(item['kode_identitas'])

            user = User.objects.create_user(
                item['kode_identitas'],
                item['kode_identitas'] + '@localhost',
                '__random123__'
            )

            profile = Profile.objects.create(
                user=user,
                kode_identitas=item['kode_identitas'],
                first_name=item['first_name'],
                last_name=item['last_name'],
                image_url=item['image_url'],
                email=item['email'],
                batch=item['batch'],
                profile_url=item['profile_url'][:150],
            )

            expertise_num = randint(1, 3)
            for i in range(expertise_num):
                x = i + 1
                Expertise.objects.create(user=profile, expert=item['expertise_' + str(x)][:25])
