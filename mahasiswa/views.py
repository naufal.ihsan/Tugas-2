from __future__ import unicode_literals
from django.http import HttpResponse
from django.contrib.auth import logout, authenticate, login as auth_login, logout as auth_logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse,HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.contrib import messages
from .models import Profile,Expertise,Status
from .utils import *
from .api_riwayat import get_riwayat
import json

from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required

response = {}

LOGIN_URL = '/mahasiswa/login'


@login_required(login_url=LOGIN_URL)
def index(request):
    return profile(request)

def mahasiswa_json(request):
    serialized = [ serialize_mahasiswa_obj(item) for item in Profile.objects.all() ]
    return JsonResponse({'data': serialized})


@login_required(login_url=LOGIN_URL)
def profile(request):
    response['user'] = request.user.profile
    response['riwayatQu'] = get_riwayat().json()
    response['statuses'] = Status.objects.all()
    response['num_feeds'] = Status.objects.all().count()
    response['latest_post'] = ''
    if Status.objects.first() is not None:
        response['latest_post'] = Status.objects.first().status
    response['expertises'] = request.user.profile.expertises.all()
    html = 'profile/profile.html'
    return render(request, html, response)

def edit_profile(request):
    html = 'edit/update_profile.html'
    return render(request, html, response)

@csrf_exempt
@login_required(login_url=LOGIN_URL)
@require_http_methods(['POST'])
def save_to_database(request):
    first_name  = request.POST['firstName']
    last_name   = request.POST['lastName']
    image_url   = request.POST['imageUrl']
    email       = request.POST['email']
    profile_url = request.POST['profileUrl']

    request.user.profile.first_name = first_name
    request.user.profile.last_name = last_name
    request.user.profile.image_url = image_url
    request.user.profile.email = email
    request.user.profile.profile_url = profile_url
    request.user.profile.save()
    return JsonResponse({'save':'success'})


@require_http_methods(['POST'])
def post_status(request):
    status = request.POST['status']
    Status.objects.create(status=status, user=request.user)
    return HttpResponseRedirect(reverse('mahasiswa:profile'))

def home(request):
    return render(request, 'homepage/home.html')


@csrf_exempt
@login_required(login_url=LOGIN_URL)
@require_http_methods(['POST'])
def save_expertise(request):
    expertise = request.POST['expertise']
    level     = request.POST['level']
    expert = Expertise.objects.create(user=request.user.profile,expert=expertise,level=level)
    return JsonResponse({'save':'success'})


@csrf_exempt
def login(request):
    if request.method == 'POST':
        SSO_USERNAME = request.POST['username']
        SSO_PASSWORD = request.POST['password']

        user = authenticate(username=SSO_USERNAME, password=SSO_PASSWORD)
        if user is not None:
            auth_login(request, user)
            return HttpResponseRedirect(reverse('mahasiswa:index'))
        else:
            messages.error(request, 'password salah :(')
            return HttpResponseRedirect(reverse('mahasiswa:login'))
    else:
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('mahasiswa:index'))

        return render(request, 'homepage/login.html')

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('home'))
