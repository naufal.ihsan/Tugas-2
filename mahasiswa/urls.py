from django.conf.urls import url

from mahasiswa.views import (
    index, profile, edit_profile,save_to_database,save_expertise, login,
    logout, post_status, mahasiswa_json )

urlpatterns = [
    url(r'^$',index, name='index'),

    url(r'^profile/$', profile, name='profile'),
    url(r'^profile/edit/$', edit_profile, name='edit'),
    url(r'^profile/save/$', save_to_database, name='save'),
    url(r'^profile/save/expertise/$', save_expertise, name='save_expertise'),

    url(r'^status/add/$', post_status, name='add_status'),

    url(r'^api/students/$', mahasiswa_json, name='mahasiswa-json'),

    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
]
