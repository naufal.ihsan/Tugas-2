var counter = 0;

var lang = [
    {"id":0,"text":"Java"},
    {"id":1,"text":"C#"},
    {"id":2,"text":"Python"},
    {"id":3,"text":"Erlang"},
    {"id":4,"text":"Kotlin"},
];

var level = [
    {"id":0,"text":"Beginner","fontColor":"#FAFAFA"},
    {"id":1,"text":"Intermediate","fontColor":"#FAFAFA"},
    {"id":2,"text":"Advance","fontColor":"#FAFAFA"},
    {"id":3,"text":"Expert","fontColor":"#FAFAFA"},
    {"id":4,"text":"Legend","fontColor":"#212121"},
];

$(document).ready(function() {
  localStorage.setItem("lang",JSON.stringify(lang));
  localStorage.setItem("level",JSON.stringify(level));

  var select_lang = JSON.parse(localStorage.getItem("lang"));
  var select_expertise = JSON.parse(localStorage.getItem("level"));

  var arrayExpertise = []
  var arrayLevel = []

  $('#lang').select2({
    'placeholder' : "Select Language",
    'data' : select_lang
  });

  $('#expertise').select2({
      placeholder : "Select Expertise",
      data : select_expertise
  });

  $('.add').on('click', function(){
    counter++;

    $('.wrapper-expertise').append(
      '<div class="lang-picker">'+
            '<select class="my-select col-md-3" id="lang_'+ counter +'">'+
                '<option></option>'+
            '</select>'+
      '</div><br>'+
      '<div class="expertise-picker">'+
            '<select class="my-select col-md-3" id="expertise_'+ counter +'">'+
                '<option></option>'+
            '</select>'+
      '</div><br>'
    );

    $('#lang_' + counter).select2({
      'placeholder' : "Select Language",
      'data' : select_lang
    });

    $('#expertise_' + counter).select2({
        placeholder : "Select Expertise",
        data : select_expertise
    });
  });


  $('.save').on('click', function(){  // sesuaikan class button
    var data = null , data1 = null;
    for(i = 0; i <= counter;i++){
      if(i == 0){
        data = $('#lang').select2('data');
        data1 =  $('#expertise').select2('data');
        arrayExpertise.push(data[0].text);
        arrayLevel.push(data1[0].text);
      }else{
        data = $('#lang_'+i).select2('data');
        data1 =  $('#expertise_'+i).select2('data');
        arrayExpertise.push(data[0].text);
        arrayLevel.push(data1[0].text);
      }
    }

    for(i =0 ;i <= counter;i++){
      insert_expertise_to_database(String(arrayExpertise[i]),String(arrayLevel[i]));
    }
  });

  var insert_expertise_to_database = function(expertise,level){
    var myUrl = 'https://collegestudent.herokuapp.com/mahasiswa/profile/save/expertise/';
    var proxy = 'https://cors-anywhere.herokuapp.com/';
    $.ajax({
        method: "POST",
        url: proxy + myUrl,
        data: {
          expertise: expertise,
          level: level,
        },
        success : function (user) {
            console.log(user);
        },
        error : function (error) {
            console.log(error);
        }
    });
};

});
