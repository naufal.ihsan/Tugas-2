from django import template

register = template.Library()

@register.filter
def lang(value):
    values = value.split(" ")
    return values[-1]
